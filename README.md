# Moodle client

Work in progress : An cli client for Toulouse-INP's moodle written in ocaml.

This work is in heavy developpment and not ready for end user yet. Feel free to contribute ! ;)

![demonstration picture showing output of cli for asr computer architecture page](./demo.png)

## End-goal

- Automatic connection through Toulouse-INP's cas
- Provide a way to access moodle files through script and without web (because moodle web interface is slow)
- Maybe: provide an interactive cli to browse moodle tree
- Maybe: provide a Fuse filesystem to browse moodle tree

To my mind, I think I will go to the fuse filesystem using ocamlfuse. But if it's too difficult I will fallback to write an interactive cli.

## What's work for now ?

This section may be not up to date. But at time of writting the following features are present:

- Connect to moodle through cas using the "cas-get.sh" bash script from bash-utilities/ folder (you must install jq and pup tools). The moodle token will be written in stdout and valid for some hours
- Browse any moodle page and give an easy to understand output (see demo.png)
- Give a moodle token to the ocaml application using env variable "TOKEN" (necessary to access authenticated content)
- You will find in the folder "bash-utilities" a list of scripts I wrote for experimenting with moodle. They shouldn't actually be used but are interesting to understand the logic. They are not robust and will be removed as soon as the ocaml rewrite will be successful.
