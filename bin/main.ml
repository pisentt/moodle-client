open Soup.Infix
open Lwt.Infix

(** print the help text to stdin *)
let print_help () =
  Printf.printf "usage: %s <base_url>\n" Sys.argv.(0);
  print_endline
    "if you wish to use the ocamlfuse version, set the OCAMLFUSE env variable \
     to any value and add the mountpoint at the end of the command"

(** value of timeout for network operations *)
let timeout = 5.

(** Read moodle token from environnment variables,
    return an option of the token value *)
let moodle_token =
  Sys.getenv_opt "TOKEN" |> function
  | Some t -> Some t
  | None ->
      print_endline
        "Warning: env variable 'TOKEN' not set. Continue without authentication";
      None

(** Choose fuse version if the ENV variable "OCAMLFUSE" is set
  *)
let choose_ocamlfuse =
  Sys.getenv_opt "OCAMLFUSE" |> function Some _ -> true | None -> false

(** Read the url to fetch (a moodle page) from command line
    and raise error if missing *)
let url =
  if Array.length Sys.argv >= 2 then Sys.argv.(1)
  else (
    print_endline "Error: missing url";
    print_help ();
    exit 1)

(** Convert a string to an absolute direct file path by resolving "./", "../"
  @param path_str the string that contain the path query by the user
  @return a list of folder names recursively without "./" or "../"
  *)
let str_to_filepath path_str =
  let rec aux path_stack path_remaining =
    match (path_stack, path_remaining) with
    | _, [] -> List.rev path_stack
    | _, "" :: t | _, "." :: t -> aux path_stack t
    | [], ".." :: t2 -> aux [] t2
    | _ :: t1, ".." :: t2 -> aux t1 t2
    | _, h :: t -> aux (h :: path_stack) t
  in
  aux [] (String.split_on_char '/' path_str)

(** test if the string s2 is a substring of s1
    @param s1 the base string
    @param s2 the string that could be a substring of s1
    @result true if s2 is a substring of s1; false otherwise
  *)
let contains s1 s2 =
  try
    let len = String.length s2 in
    for i = 0 to String.length s1 - len do
      if String.sub s1 i len = s2 then raise Exit
    done;
    false
  with Exit -> true

(** Construct the header for http requests depending
    if a token is provided or not
    @param compression optional, default to false.
           If true: add header to accept gzip compression
    @param token an option of the moodle token
    @return an empty header if the token is None or a header
            with the MoodleSession cookie set otherwise
  *)
let construct_header ?(compression = false) token =
  (match token with
  | Some t ->
      Cohttp.Header.init_with "Cookie" (Printf.sprintf "MoodleSession=%s" t)
  | None -> Cohttp.Header.init ())
  |> fun h ->
  if compression then Cohttp.Header.add h "Accept-Encoding" "gzip" else h

type 'a request_finished =
  [ `Ok of 'a  (** Successfuly run operation with result *)
  | `Need_Authentication of Uri.t  (** Authentication failed when run request *)
  | `Error of Uri.t
    (** There was an error with the operation
  but we do not precise the type of error *)
  ]
(** A type to get status of a finished request *)

(** The type for the moodle filesystem tree *)
type filetree =
  | Resource of
      (token:string option ->
      (Uri.t
      * string
      * ((char, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t ->
        int ->
        (int, Unix.error) result Lwt.t)
      * int Lwt.t lazy_t)
      request_finished
      Lwt.t)
      (** A file you can download identified by its url and its name.
          They are defined as lazy_request because we need to make a request
          to resolve the redirection before getting actual values *)
  | Folder of
      Uri.t
      * string
      * (token:string option -> filetree list request_finished Lwt.t)
      (** A web page containing useful links identified by it's url and it's
          title and the list of contained links
          (which is a lazy_request list of filetrees) *)

(** Function to add timeout to any lwt promise
    @param time the value of the timeout
    @param promise the promise to wait for
    @return `Done <value> if the promise ended in time or `Timeout
  *)
let compute ~timeout ~promise =
  Lwt.pick
    [
      (promise >|= fun v -> `Done v);
      (Lwt_unix.sleep timeout >|= fun () -> `Timeout);
    ]

(** Verify the moodle response don't failed to authenticate
    and then return the good value or an error
    @param response the response to analyse
    @param err_val the value to return if authentification is not good
    @param ok_val the value to return if the authentification is good.
           Usually, just continue execution of current task
  *)
let assert_auth_ok response ~err_val ~ok_val =
  match Cohttp.Header.get (Cohttp.Response.headers response) "location" with
  | None -> ok_val
  | Some redirection ->
      if contains redirection "/enrol/index.php" then err_val else ok_val

(* ====================================================================








  *)

module Lwt_arraylist () = struct
  open Bigarray

  let buf_len = 40000000
  let content = Array1.create char c_layout buf_len
  let size = ref 0
  let list_notify = ref []
  let finished_download = ref false

  let append str =
    let str_len = String.length str in
    (* copy the string at the end *)
    String.iteri
      (fun i c ->
        let n = !size + i in
        if n < buf_len then content.{n} <- c)
      str;
    size := !size + str_len;
    print_int !size;
    print_newline ();
    (* notify all waiting thread that ask for less than !size *)
    list_notify :=
      List.fold_left
        (fun acc (i, cond) ->
          if i <= !size then (
            Lwt_condition.signal cond !size;
            acc)
          else (i, cond) :: acc)
        [] !list_notify

  let read buf offset =
    let len = Array1.dim buf in
    let threshold = offset + len in
    (if threshold > !size && not !finished_download then (
       let wait_condition = Lwt_condition.create () in
       list_notify := (threshold, wait_condition) :: !list_notify;
       (* wait for data to arrive in buffer *)
       Lwt_condition.wait wait_condition)
     else Lwt.return !size)
    >|= fun _ ->
    if threshold < !size then (
      print_endline "ok !";
      Array1.blit (Array1.sub content offset len) (Array1.sub buf 0 len);
      len)
    else (
      (* our buffer is bigger than the content *)
      print_endline "bigger !";
      let len = max 0 (!size - offset) in
      Array1.blit (Array1.sub content offset len) (Array1.sub buf 0 len);
      len)

  let stop_fill () =
    (* notify all waiting threads *)
    finished_download := true;
    List.iter (fun (_, cond) -> Lwt_condition.signal cond !size) !list_notify;
    list_notify := []
end

let get_read_fun ~token url =
  let defer_req =
    (* Don't download the file content immediatly *)
    lazy
      (compute ~timeout
         ~promise:
           (Cohttp_lwt_unix.Client.get
              ~headers:(construct_header ~compression:false token)
              url)
       >>= function
       | `Timeout -> Lwt.return (`Error url)
       | `Done (response, body) ->
           (* First, we verify that there is no authenticate failure,
              in this case we stop processing and return Need_Authentication *)
           assert_auth_ok response
             ~err_val:(Lwt.return (`Need_Authentication url))
             ~ok_val:
               (match body with
               | `Stream stream ->
                   let open Lwt_arraylist () in
                   let _ = Lwt_stream.iter append stream >|= stop_fill in
                   Lwt.return (`Ok read)
               | `Empty | `String _ | `Strings _ -> Lwt.return (`Error url)))
  in
  fun x y ->
    Lazy.force defer_req >>= function
    | `Ok f ->
        let%lwt n_read = f x y in
        Lwt.return (Ok n_read)
    | `Need_Authentication _ -> Lwt.return (Error Unix.EACCES)
    | `Error _ -> Lwt.return (Error Unix.ENOENT)

let print_bigarray arr size : unit =
  print_int size;
  for i = 0 to size - 1 do
    print_char arr.{i}
  done;
  print_newline ()

(*
let main_test () =
  let open Lwt_arraylist () in
  let read_test offset len =
    let buf = Bigarray.(Array1.create char c_layout len) in
    read buf offset >|= print_bigarray buf
  in
  compute ~timeout
    ~promise:
      (Cohttp_lwt_unix.Client.get
         ~headers:(construct_header ~compression:false moodle_token)
         (Uri.of_string
            "https://moodle-n7.inp-toulouse.fr/course/index.php?categoryid=367"))
  >>= function
  | `Timeout -> failwith "TIMEOUT"
  | `Done (_, body) ->
      let _ =
        match body with
        | `Stream stream -> Lwt_stream.iter append stream >|= stop_fill
        | `Empty | `String _ | `Strings _ -> failwith "unspported"
      in
      Lwt_list.iter_p
        (fun x -> x)
        [ read_test 0 10; read_test 0 100; read_test 10000 10 ]
      >>= fun () ->
      Lwt_list.iter_p (fun x -> x) [ read_test 8 10; read_test 100 100 ]

let () =
  Lwt_main.run (main_test ());
  exit 0
*)

(* ====================================================================








  *)

let get_file_size ~token url =
  (* The goal is to request the last 100 bytes of the file to force the server
     to set the filesize in response headers *)
  compute ~timeout
    ~promise:
      (Cohttp_lwt_unix.Client.get
         ~headers:
           ( construct_header ~compression:false token |> fun h ->
             Cohttp.Header.add h "Range" "bytes=-1" )
         url)
  >>= function
  | `Timeout -> failwith "timeout"
  | `Done (response, _) -> (
      (* content-range: bytes 670792-670891/670892 *)
      match
        Cohttp.Header.get (Cohttp.Response.headers response) "content-range"
      with
      | None -> Lwt.return 0
      | Some s -> (
          match String.split_on_char '/' s with
          | _ :: t :: _ -> Lwt.return (int_of_string t)
          | _ -> Lwt.return 0))

(** Fetch a resource link with the "head" method to resolve the redirection
    and get the actual link and filename of the resource
    @param token the token to authenticate if needed
    @param url the url to fetch
    @return the actual url and name to download the resource
    or None if there are not found in the http response
  *)
let resource_resolve_redirection ~token url =
  compute ~timeout
    ~promise:(Cohttp_lwt_unix.Client.head ~headers:(construct_header token) url)
  >|= function
  | `Timeout -> `Error url
  | `Done response ->
      Cohttp.Header.fold
        (fun hname hval acc ->
          match hname with
          | "location" ->
              if contains hval "/enrol/index.php" then `Need_Authentication url
              else
                let new_url = Uri.of_string hval in
                `Ok
                  ( new_url,
                    Filename.basename (Uri.path new_url),
                    get_read_fun ~token new_url,
                    lazy (get_file_size ~token new_url) )
          | _ -> acc)
        (Cohttp_lwt.Response.headers response)
        (`Error url)

(** Force resolution of a lazy filetree.Resource
    @param resource the lazy_request defining the resource
           (that can be a previously failed lazy_request)
    @return the resolved lazy_request that can also be a failure
  *)
let lazy_resource_force url :
    token:string option ->
    (Uri.t
    * string
    * ((char, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t ->
      int ->
      (int, Unix.error) result Lwt.t)
    * int Lwt.t lazy_t)
    request_finished
    Lwt.t =
  let lazy_result = ref None in
  let aux ~token =
    match !lazy_result with
    | Some x -> (
        x >|= fun req_result ->
        match req_result with
        | `Ok _ -> req_result
        | `Error _ | `Need_Authentication _ ->
            (* if any error, force to redo the request next time *)
            lazy_result := None;
            req_result)
    | None -> (
        let pending = resource_resolve_redirection ~token url in
        (* Save pending request so another call before resolution
           won't do another request *)
        lazy_result := Some pending;
        pending >|= fun req_result ->
        match req_result with
        | `Ok _ ->
            lazy_result := Some (Lwt.return req_result);
            req_result
        | `Error _ | `Need_Authentication _ ->
            (* if any error, force to redo the request next time *)
            lazy_result := None;
            req_result)
  in
  aux

(** Function to decompress a gzip string using decompress library.
    It's a direct copy-paste form decompress documentation
  *)
let inflate_string str =
  let i = De.bigstring_create De.io_buffer_size in
  let o = De.bigstring_create De.io_buffer_size in
  let r = Buffer.create 0x1000 in
  let p = ref 0 in
  let refill buf =
    let len = min (String.length str - !p) De.io_buffer_size in
    Bigstringaf.blit_from_string str ~src_off:!p buf ~dst_off:0 ~len;
    p := !p + len;
    len
  in
  let flush buf len =
    let str = Bigstringaf.substring buf ~off:0 ~len in
    Buffer.add_string r str
  in
  match Gz.Higher.uncompress ~refill ~flush i o with
  | Ok m -> Ok (m, Buffer.contents r)
  | Error _ as err -> err

(** Convert the Cohttp_lwt body object to a readable string and decompress
    gzip data if needed
    @param response the response to a http request
    @param body the body got in response to a http request
    @return a string Option with the content of the body decoded and also
            inflated if data was compressed with gzip.
            If we encounter any error we return None
 *)
let http_body_to_string ~response ~body =
  let%lwt bodystring = Cohttp_lwt.Body.to_string body in
  let is_compressed =
    Cohttp.Header.get (Cohttp.Response.headers response) "content-encoding"
    |> function
    | Some "gzip" -> true
    | _ -> false
  in
  Lwt.return
    (if is_compressed then
       match inflate_string bodystring with
       | Ok (_, s) -> Some s
       | Error _ -> None
     else Some bodystring)

(** Parse a link to a filetree element
    @param link an option to a link as returned by lambdasoup
    @param titlelist of string with all the text of the link
           as returned by lambdasoup
    @return Some filetree element (Resource or Folder) with all lazy_request
            equal to "Defered" or None if the link is unsupported
  *)
let rec link_to_filetree link titlelist =
  match (link, titlelist) with
  | Some link, titlelist ->
      let return_resource () =
        Resource (lazy_resource_force (Uri.of_string link))
      and return_folder () =
        let uri = Uri.of_string link in
        Folder (uri, String.concat " " titlelist, lazy_folder_force uri)
      in
      if contains link "&stopjsnav=1" then None
      else if contains link "&skipcheck=1" then None
      else if contains link "/course/" then Some (return_folder ())
      else if contains link "/mod/folder/" then Some (return_folder ())
      else if contains link "/mod/resource/" then Some (return_resource ())
      else if contains link "/mod_folder/content/" then
        Some (return_resource ())
      else None
  | None, _ -> None

and folder_fetch_content ~token url : filetree list request_finished Lwt.t =
  compute ~timeout
    ~promise:
      (Cohttp_lwt_unix.Client.get
         ~headers:(construct_header ~compression:true token)
         url)
  >>= function
  | `Timeout -> Lwt.return (`Error url)
  | `Done (response, body) ->
      (* First, we verify that there is no authenticate failure,
         in this case we stop processing and return Need_Authentication *)
      assert_auth_ok response
        ~err_val:(Lwt.return (`Need_Authentication url))
        ~ok_val:
          ((* Simple operators to stop evaluation at the
              first function that return None and then return `Error url *)
           let ( |>> ) a f = a |> function None -> `Error url | Some b -> f b
           and ( >>>= ) a f =
             a >|= function None -> `Error url | Some b -> f b
           in
           (* Find the main div on the page, if it's missing, raise error *)
           http_body_to_string ~response ~body >>>= fun s ->
           Soup.parse s $? "div[role=main]" |>> fun div ->
           (* Get all links of the current div *)
           div $$ "a" |> Soup.to_list |> fun l ->
           `Ok
             (* Convert all links to filetree element and keep only
                non "None" results *)
             (List.fold_left
                (fun acc x ->
                  match
                    link_to_filetree (Soup.attribute "href" x)
                      (Soup.trimmed_texts x)
                  with
                  | None -> acc
                  | Some y -> y :: acc)
                [] l))

(** Fetch the page related to a moodle folder and parse content
    to construct the children filetree elements
    @param token an option with maybe a moodle token
    @param lazy_content a lazy_request to the content of the folder
    @return Ok <list of filetree elements> (content of the folder)
            or a failed lazy_request
  *)
and lazy_folder_force url :
    token:string option -> filetree list request_finished Lwt.t =
  let lazy_result = ref None in
  let aux ~token =
    match !lazy_result with
    | Some x -> (
        x >|= fun req_result ->
        match req_result with
        | `Ok _ -> req_result
        | `Error _ | `Need_Authentication _ ->
            (* if any error, force to redo the request next time *)
            lazy_result := None;
            req_result)
    | None -> (
        let pending = folder_fetch_content ~token url in
        (* Save pending request so another call before resolution
           won't do another request *)
        lazy_result := Some pending;
        pending >|= fun req_result ->
        match req_result with
        | `Ok _ ->
            lazy_result := Some (Lwt.return req_result);
            req_result
        | `Error _ | `Need_Authentication _ ->
            (* if any error, force to redo the request next time *)
            lazy_result := None;
            req_result)
  in
  aux

(* test to add fuse support but only for folders for now
   (the rest will come after big code refactor) *)

(** return content of the current folder
    @param token Some moodle_token to use authentication or None
    @param lazy_content the lazy object to query content of current folder
    @return the updated lazy_content object to make future calls faster
  *)
let return_folder_content ~token
    (curr_folder_content : filetree list request_finished) :
    [> `Content of filetree list | `Error | `Not_Found | `Permission_denied ]
    Lwt.t =
  match curr_folder_content with
  | `Ok items ->
      Lwt_list.map_p
        (function
          | Folder (url, name, lazy_content) ->
              (* Preload folder content by running request in background *)
              ignore (lazy_content ~token);
              Lwt.return (Folder (url, name, lazy_content))
          | Resource lazy_req -> Lwt.return (Resource lazy_req))
        items
      >|= fun new_items -> `Content new_items
  | `Need_Authentication _ -> Lwt.return `Permission_denied
  | `Error _ -> Lwt.return `Error

(** Browse recursively folders to follow a path an display content of last folder
    @param token Some moodle_token to use authentication or None
    @param lazy_content the lazy object to query content of current folder
    @param filepath a path to the folder to display on the screen
    @return the updated lazy_content object to make future calls faster and the result (the Not_Found, Resource or Folder)
  *)
let rec search_folder ~token
    (curr_folder_content : filetree list request_finished)
    (filepath : string list) =
  match filepath with
  | [] -> return_folder_content ~token curr_folder_content
  | h :: t -> (
      match curr_folder_content with
      | `Ok items ->
          Lwt_list.fold_left_s
            (fun found curr ->
              match curr with
              | Folder (_, name, lazy_content) ->
                  if name = h then
                    let%lwt inner_folder_content = lazy_content ~token in
                    search_folder ~token inner_folder_content t
                  else Lwt.return found
              | Resource _ -> Lwt.return found)
            `Not_Found items
      | `Need_Authentication _ -> Lwt.return `Permission_denied
      | `Error _ -> Lwt.return `Error)

(** Print content of the current folder
    @param token Some moodle_token to use authentication or None
    @param lazy_content the lazy object to query content of current folder
    @return the updated lazy_content object to make future calls faster
  *)
let print_folder_content ~token
    (content :
      [> `Content of filetree list | `Error | `Not_Found | `Permission_denied ])
    : unit Lwt.t =
  let%lwt _ =
    match content with
    | `Content items ->
        Lwt_list.iter_p
          (function
            | Folder (url, name, lazy_content) ->
                (* Preload folder content by running request in background *)
                let _ = lazy_content ~token in
                Lwt_io.printf "📂 %s (%s)\n" name (Uri.to_string url)
            | Resource lazy_req -> (
                lazy_req ~token >>= function
                | `Ok (url, name, _, _) ->
                    Lwt_io.printf "📄 %s (%s)\n" name (Uri.to_string url)
                | `Need_Authentication _ ->
                    Lwt_io.printl "🚫 Error: No authorized to access ressource"
                | `Error _ -> Lwt_io.printl "❗ Error: Failed to query resource"))
          items
    | `Not_Found -> Lwt_io.printl "Error: folder not found"
    | `Permission_denied ->
        Lwt_io.printl
          "Error: Authentication failed, please retry with a valid token"
    | `Error -> Lwt_io.printl "Error: Internal failure"
  in
  (* To be sure everything is printed on screen before continue *)
  Lwt_io.(flush stdout)

(** The uri of the root folder *)
let root_uri = Uri.of_string url

(** the function to get root folder content *)
let lazy_root_folder = lazy_folder_force root_uri

(** Return the first element found from a filetree list with the targe name.
    All network requests are run simultaneously we don't wait for all pending
    requests to finish if the target name is found yet
    @param token Some token or None
    @param content_list the filetree list to browse
    @param target_filename the filename we search
           (we don't know if it's a folder or resource)
    @return Some filetree if name is found or None if not found
            (we do not precise if there is some network errors)
  *)
let find_content_element_with_name ~token content_list target_filename =
  let promises_list =
    (* We use list.map to run all promises in parallel *)
    List.map
      (fun x ->
        match x with
        | Folder (_, name, _) ->
            Lwt.return (if name = target_filename then Some x else None)
        | Resource lazy_metadata -> (
            lazy_metadata ~token >|= function
            | `Need_Authentication _ | `Error _ -> None
            | `Ok (_url, name, _, _) ->
                if name = target_filename then Some x else None))
      content_list
  in
  (* Auxiliary function that return the content of the first promises
     with the correct filename that resolve *)
  let rec select_first_found promises_list =
    (* Get all promises that are finished yet *)
    Lwt.nchoose_split promises_list >>= fun (finished, pending) ->
    let maybe_found =
      List.fold_left
        (fun acc x -> match x with Some _ -> x | None -> acc)
        None finished
    in
    match maybe_found with
    | Some _ ->
        Lwt.return maybe_found
        (* If found in one of the already finished request, return *)
    | None -> (
        match pending with
        | [] -> Lwt.return None
        | pending ->
            select_first_found pending
            (* In not found yet, wait for some other promises to finish *))
  in
  select_first_found promises_list

(** Find any file (Resource or Folder) from it's path, and return it *)
let find_any_file path =
  let maybefilename, folder =
    (* get the last element of the list for maybefilename and folder
       is all previous elements (in the good order) *)
    List.rev path |> function [] -> (None, []) | h :: t -> (Some h, List.rev t)
  in
  match maybefilename with
  | None ->
      (* in this case we ask for root folder *)
      Lwt.return (Some (Folder (root_uri, "", lazy_root_folder)))
  | Some filename -> (
      let%lwt parent_folder_content =
        lazy_root_folder ~token:moodle_token >>= fun root_content ->
        search_folder ~token:moodle_token root_content folder
      in
      match parent_folder_content with
      | `Not_Found -> Lwt.return None
      | `Permission_denied | `Error -> Lwt.return None
      | `Content parent_content_list ->
          find_content_element_with_name ~token:moodle_token parent_content_list
            filename
          >>= Lwt.return)

(** Main application loop 
    @param token Some moodle_token to use authentication or None
    @param roottree a lazy object representing the full moodle file tree
    @param path_str a string with the path of the current folder
  *)
let rec main_loop ~token path_str =
  flush_all ();
  print_string path_str;
  print_string " >> ";
  flush_all ();
  Lwt_io.(read_line_opt stdin) >>= function
  | None -> Lwt.return_unit (* exit the app with ctrl+d *)
  | Some command -> (
      match String.split_on_char ' ' command with
      | "ls" :: t ->
          let filepath =
            str_to_filepath (path_str ^ "/" ^ String.concat " " t)
          in
          let%lwt result =
            lazy_root_folder ~token >>= fun root_content ->
            search_folder ~token root_content filepath
          in
          print_folder_content ~token result >>= fun () ->
          main_loop ~token ("/" ^ String.concat "/" filepath)
      | "cd" :: t ->
          let filepath =
            str_to_filepath (path_str ^ "/" ^ String.concat " " t)
          in
          (* preload folder content *)
          let _ =
            lazy_root_folder ~token >>= fun root_content ->
            search_folder ~token root_content filepath
          in
          main_loop ~token ("/" ^ String.concat "/" filepath)
      | "cat" :: t ->
          (let filepath =
             str_to_filepath (path_str ^ "/" ^ String.concat " " t)
           in
           let%lwt found = find_any_file filepath in
           match found with
           | Some (Folder _) | None -> failwith "error: it's a folder\n"
           | Some (Resource lazy_metadata) -> (
               let%lwt _, _, read_fun, _ =
                 lazy_metadata ~token:moodle_token >>= function
                 | `Ok x -> Lwt.return x
                 | `Need_Authentication _ ->
                     failwith "error: authentication error"
                 | `Error _ -> failwith "error: internal error"
               in
               let content =
                 Bigarray.Array1.create Bigarray.char Bigarray.c_layout 1000
               in
               read_fun content 0 >|= function
               | Ok n -> print_bigarray content n
               | Error _ -> Printf.printf "error: read error"))
          >>= fun () -> main_loop ~token:(Some (String.concat " " t)) path_str
      | "token" :: t -> main_loop ~token:(Some (String.concat " " t)) path_str
      | _ ->
          Printf.printf "Unknown command: %s\n" command;
          main_loop ~token path_str)

(* Start application loop *)
let () =
  if not choose_ocamlfuse then (
    Lwt_main.run (main_loop ~token:moodle_token "");
    exit 0)

let do_readdir path _ =
  let%lwt result =
    lazy_root_folder ~token:moodle_token >>= fun root_content ->
    search_folder ~token:moodle_token root_content (str_to_filepath path)
  in
  match result with
  | `Not_Found -> raise (Unix.Unix_error (ENOENT, "readdir", path))
  | `Permission_denied | `Error ->
      raise (Unix.Unix_error (EACCES, "readdir", path))
  | `Content content_list ->
      (* run all network request in parallel *)
      Lwt_list.map_p
        (function
          | Folder (_, name, _) -> Lwt.return (Some name)
          | Resource lazy_metadata -> (
              lazy_metadata ~token:moodle_token >|= function
              | `Ok (_url, name, _, _) -> Some name
              | _ -> None))
        content_list
      >|= fun result ->
      (* combine results with "." and ".." that are always present *)
      List.fold_left
        (fun acc x -> match x with Some value -> value :: acc | None -> acc)
        [ "."; ".." ] result

let default_stats = Unix.LargeFile.stat "."

let do_getattr path : Unix.LargeFile.stats Lwt.t =
  let%lwt found = find_any_file (str_to_filepath path) in
  match found with
  | None -> raise (Unix.Unix_error (ENOENT, "getattr", path))
  | Some (Folder _) -> Lwt.return default_stats
  | Some (Resource lazy_metadata) ->
      let%lwt filesize =
        lazy_metadata ~token:moodle_token >>= function
        | `Ok (_, _, _, lazy_size) -> Lazy.force lazy_size
        | _ -> Lwt.return 0
      in
      Lwt.return
        {
          default_stats with
          st_nlink = 1;
          st_kind = S_REG;
          st_perm = 0o444;
          st_size = Int64.of_int filesize;
          (* We can't know the size of the file yet. !! Don't forget to
             set direct_io flag in open to be able to read content !! *)
        }

let do_fopen path _flags =
  let%lwt found = find_any_file (str_to_filepath path) in
  match found with
  | None -> raise (Unix.Unix_error (ENOENT, "open", path))
  | Some (Folder _) ->
      Lwt.return (None, false)
      (* I don't know if direct_io
         has an effect for folders *)
  | Some (Resource lazy_metadata) ->
      let%lwt filesize =
        lazy_metadata ~token:moodle_token >>= function
        | `Ok (_, _, _, lazy_size) -> Lazy.force lazy_size
        | _ -> Lwt.return 0
      in
      Lwt.return (None, filesize == 0)
(* IMPORTANT: we must return true for resources
   to set direct_io because we said in get_attr
   that the resources have a st_size of 0 *)

let do_read path buf (ofs : int64) _ =
  let%lwt found = find_any_file (str_to_filepath path) in
  match found with
  | Some (Folder _) | None -> raise (Unix.Unix_error (ENOENT, "read", path))
  | Some (Resource lazy_metadata) -> (
      let%lwt _, _, read_fun, _ =
        lazy_metadata ~token:moodle_token >>= function
        | `Ok x -> Lwt.return x
        | `Need_Authentication _ ->
            raise (Unix.Unix_error (EACCES, "read", path))
        | `Error _ -> raise (Unix.Unix_error (EAGAIN, "read", path))
      in
      read_fun buf (Int64.to_int ofs) >|= function
      | Ok n -> n
      | Error _ -> raise (Unix.Unix_error (EAGAIN, "read", path)))

(** We return the sys args without the first because it's the url to moodle *)
let fuse_array_args =
  Array.init
    (Array.length Sys.argv - 1)
    (fun i -> if i = 0 then Sys.argv.(0) else Sys.argv.(i + 1))

let _ =
  Fuse.main fuse_array_args
    {
      Fuse.default_operations with
      init =
        (fun () ->
          Printf.printf "filesystem started\n%!";
          Thread.create
            (fun () ->
              Lwt_main.run
                (let stop_condition = Lwt_condition.create () in
                 Lwt_condition.wait stop_condition))
            ()
          |> ignore);
      getattr = (fun a -> Lwt_preemptive.run_in_main (fun () -> do_getattr a));
      readdir =
        (fun a b -> Lwt_preemptive.run_in_main (fun () -> do_readdir a b));
      fopen = (fun a b -> Lwt_preemptive.run_in_main (fun () -> do_fopen a b));
      read =
        (fun a b c d -> Lwt_preemptive.run_in_main (fun () -> do_read a b c d));
    }

let () = print_endline "ERROR: failed to start fuse filesystem"
