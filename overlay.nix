# overlay
final: prev:
{
  ocamlPackages = prev.ocamlPackages.overrideScope
    (final-ocaml: prev-ocaml:
      {
        ocamlfuse =
          prev-ocaml.ocamlfuse.overrideAttrs
            (old: {
              src = final.fetchFromGitHub {
                owner = "astrada";
                repo = "ocamlfuse";
                rev = "1274ce28f3d8c11dc79626e4f567c8249b09fb37";
                hash = "sha256-mR4udcHy502Kf/ioF4sj/9bJ5id+pAqQmRH8fRk4OGQ=";
              };

              # add custom patch to enable direct_io
              patches = [ ./0001-fopen-now-return-a-boolean-to-enable-direct_io-or-no.patch ];

              postPatch = ''
                substituteInPlace lib/Fuse_main.c \
                  --replace-warn "<fuse_lowlevel.h>" "<fuse/fuse_lowlevel.h>"
              '';
            });
      }
    );
}
