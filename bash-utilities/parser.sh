#!/usr/bin/env bash

# ===== parser.sh =====
# usage: parser.sh <filename> or <your_previous_command> | parser.sh
# dependencies: pup, jq and recode

# Function to process each link and create a JSON object
process_link() {
  local link_href="$1"
  local link_text="$2"
  local element_type

  if [[ "$link_href" == *"&stopjsnav=1" ]]; then
    element_type="my-other"
  elif [[ "$link_href" == *"&skipcheck=1" ]]; then
    element_type="my-other"
  elif [[ "$link_href" == *"moodle-n7.inp-toulouse.fr/course"* ]]; then
    element_type="my-folder"
  elif [[ "$link_href" == *"moodle-n7.inp-toulouse.fr/mod/folder"* ]]; then
    element_type="my-folder"
  elif [[ "$link_href" == *"moodle-n7.inp-toulouse.fr/mod/resource"* ]]; then
    element_type="my-resource"
  elif [[ "$link_href" == *"moodle-n7.inp-toulouse.fr"*"mod_folder/content"* ]]; then
    element_type="my-resource"
  else
    element_type="my-other"
  fi

  echo "{\"type\": \"$element_type\", \"my-link\": \"$link_href\", \"text\": \"$link_text\"}"
}

# Extract <a> tags and process them
json_content=$(pup 'div[role=main] a json{}' < "${1:-/dev/stdin}" | jq -r '.[] | .href as $href | (.. | .text? // empty) as $text | @sh "\($href) \($text)"' | recode utf8..java,html..java,java..utf8 | tr -d "'" | while read -r href text; do
  # Process link and create JSON object
  process_link "$href" "$text"
done | jq -s .)

# Output the JSON
echo "$json_content" | jq .
