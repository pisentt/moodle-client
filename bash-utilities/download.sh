#!/usr/bin/env bash
token="u1hlj8cl8jnd2b5n7gpsk01r6i"

# Function to process each link and create a JSON object
process_link() {
  local eltype="$1"
  local url="$2"
  local title="$3"

  if [[ "$url" == "$(cat ./url)" ]]; then
    return
  elif [[ "$eltype" == "my-folder" ]]; then
    mkdir -p "$title"
    echo "$url" > "$title/url"
  elif [[ "$eltype" == "my-resource" ]]; then
    curl --compressed -O -J -L -H "Cookie: MoodleSession=$token" "$url" &
  fi
}

jq -r '.[] | .type as $type | ."my-link" as $url | .text as $title | @sh "\($type) \($url) \($title)"' < "${1:-/dev/stdin}" | tr -d "'" | while read -r eltype url title; do
  process_link "$eltype" "$url" "$title"
done
