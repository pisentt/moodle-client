#!/usr/bin/env bash
echo "current folder: $1"
cd "$1" && (
curl --compressed -L -H "Cookie: MoodleSession=uov0gesk6d39hl0865v88v9t0i" "$(cat url)" | ./parser.sh | ./download.sh
#curl --compressed -L "$(cat url)" | ./parser.sh | ./download.sh
for d in */
do
  ./recursion.sh "$d" &
done
)
