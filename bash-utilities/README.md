# Bash utilities

This folder will diseapear in the futur as things will be reimplemented in ocaml.
There is some bash scripts to interact with moodle website with bash. Most of them are not robust and may be manually edited to be used (token are hard-coded, and of course there are all already expired in case you want to try to connect to my account).

## Brief explanation of scripts

The scripts depends mainly on these bash tools that you have to install manually on your system:
- curl (http requests)
- pup (html manipulation)
- jq (json manipulation)
- recode (utf8 string manipulation)

The scripts present are:
- cas-get.sh : connect to Toulouse-INP's cas and retrieve a moodle token (it should be robust you can use it freely)
- parser.sh : parse moodle html to retrieve links and categorised them into "my-folder" (link to another page), "my-resource" (a file you can download), "my-other" (unsupported link)
- download.sh : run commands for each links output by parser.sh depending of the link type
    - my-folder : create a new folder in current folder and a file "url" inside with the corresponding moodle url
    - my-resource : download a remote file with the original filename
    - my-other : do-nothing
- recursion.sh : call the 3 previous scripts recursively to download the entire moodle filetree (be caution that it can start a lot of child processes if you run it with the root moodle url that can result in fork bomb or a lot of network activity). **It lacks further testing. I can't garantie it won't cause infinite loop in some cases !!**
- download-file-good-name.sh : just a reminder to download a moodle resource file with good filename
- utiliser-cookie.sh : just a reminder of curl syntax to authenticate with a moodle token
